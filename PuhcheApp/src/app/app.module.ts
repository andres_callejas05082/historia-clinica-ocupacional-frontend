import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// rutas
 import { APP_ROUTING } from './app.routes';

// servicios


 // componentes
import { AppComponent } from './app.component';
import {HeaderComponent} from './components/header/header.component';
import { BodyComponent } from './components/body/body.component';
import { FooterComponent } from './components/footer/footer.component';
import { LoginComponent } from './components/login/login.component';
import { MedicoComponent } from './components/registro/medico/medico.component';
import { EmpresaComponent } from './components/registro/empresa/empresa.component';
import { AuxiliarComponent } from './components/registro/auxiliar/auxiliar.component';
import { ArlComponent } from './components/registro/arl/arl.component';
import { EpsComponent } from './components/registro/eps/eps.component';
import { AnamnesisComponent } from './components/paciente/anamnesis/anamnesis.component';
import { IdentificacionComponent } from './components/paciente/identificacion/identificacion.component';
import { ExamenfisicoComponent } from './components/paciente/examenfisico/examenfisico.component';
import { RevisionComponent } from './components/paciente/revision/revision.component';
import { DiagnosticoComponent } from './components/paciente/diagnostico/diagnostico.component';
import { TratamientoComponent } from './components/paciente/tratamiento/tratamiento.component';
import { RecomendacionesComponent } from './components/paciente/recomendaciones/recomendaciones.component';
import { CertificadoComponent } from './components/paciente/certificado/certificado.component';
import { PacienteComponent } from './components/paciente/pacient/paciente.component';
import { RegistroComponent } from './components/registro/registro/registro.component';
import { BuscarComponent } from './components/paciente/buscar/buscar.component';
import { AuxiliarpaComponent } from './components/paciente/auxiliarpa/auxiliarpa.component';
import { MedicopaComponent } from './components/paciente/medicopa/medicopa.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BodyComponent,
    FooterComponent,
    LoginComponent,
    MedicoComponent,
    EmpresaComponent,
    AuxiliarComponent,
    ArlComponent,
    EpsComponent,
    AnamnesisComponent,
    IdentificacionComponent,
    ExamenfisicoComponent,
    RevisionComponent,
    DiagnosticoComponent,
    TratamientoComponent,
    RecomendacionesComponent,
    CertificadoComponent,
    PacienteComponent,
    RegistroComponent,
    BuscarComponent,
    AuxiliarpaComponent,
    MedicopaComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTING
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
