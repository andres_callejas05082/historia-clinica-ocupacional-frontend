import { RouterModule, Routes } from '@angular/router';
import { HeaderComponent } from './components/header/header.component';
import { LoginComponent } from './components/login/login.component';
import { FooterComponent } from './components/footer/footer.component';
// paciente
import { PacienteComponent } from './components/paciente/pacient/paciente.component';
import { IdentificacionComponent } from './components/paciente/identificacion/identificacion.component';
import { AnamnesisComponent } from './components/paciente/anamnesis/anamnesis.component';
import { CertificadoComponent } from './components/paciente/certificado/certificado.component';
import { DiagnosticoComponent } from './components/paciente/diagnostico/diagnostico.component';
import { ExamenfisicoComponent } from './components/paciente/examenfisico/examenfisico.component';
import { RecomendacionesComponent } from './components/paciente/recomendaciones/recomendaciones.component';
import { RevisionComponent } from './components/paciente/revision/revision.component';
import { TratamientoComponent } from './components/paciente/tratamiento/tratamiento.component';
import { BuscarComponent } from './components/paciente/buscar/buscar.component';
import { MedicopaComponent } from './components/paciente/medicopa/medicopa.component';
import { AuxiliarpaComponent } from './components/paciente/auxiliarpa/auxiliarpa.component';

// registro
import { RegistroComponent } from './components/registro/registro/registro.component';
import { AuxiliarComponent } from './components/registro/auxiliar/auxiliar.component';
import { ArlComponent } from './components/registro/arl/arl.component';
import { EmpresaComponent } from './components/registro/empresa/empresa.component';
import { EpsComponent } from './components/registro/eps/eps.component';
import { MedicoComponent } from './components/registro/medico/medico.component';



const APP_ROUTES: Routes = [
{ path: 'inicio', component: FooterComponent},
{ path: 'registro', component: RegistroComponent, children: [
    { path: 'medico', component: MedicoComponent},
    { path: 'empresa', component: EmpresaComponent},
    { path: 'auxiliar', component: AuxiliarComponent},
    { path: 'arl', component: ArlComponent},
    { path: 'eps', component: EpsComponent},
    { path: '**', pathMatch: 'full', redirectTo: 'empresa'}
]},
{ path: 'paciente', component: PacienteComponent, children: [
    { path: 'identificacion', component: IdentificacionComponent},
    { path: 'paciente', component: PacienteComponent},
    { path: 'anamnesis', component: AnamnesisComponent},
    { path: 'fisico', component: ExamenfisicoComponent},
    { path: 'revision', component: RevisionComponent},
    { path: 'diagnostico', component: DiagnosticoComponent},
    { path: 'tratamiento', component: TratamientoComponent},
    { path: 'recomendaciones', component: RecomendacionesComponent},
    { path: 'certificado', component: CertificadoComponent},
    { path: 'busqueda', component: BuscarComponent},
    { path: 'medico', component: MedicopaComponent},
    { path: 'auxiliar', component: AuxiliarpaComponent},
    { path: '**', pathMatch: 'full', redirectTo: 'busqueda'}
]},
{ path: 'login', component: LoginComponent},
{ path: '**', pathMatch: 'full', redirectTo: 'Inicio'}
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES ,  { useHash: true } );

