import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicopaComponent } from './medicopa.component';

describe('MedicopaComponent', () => {
  let component: MedicopaComponent;
  let fixture: ComponentFixture<MedicopaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicopaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicopaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
