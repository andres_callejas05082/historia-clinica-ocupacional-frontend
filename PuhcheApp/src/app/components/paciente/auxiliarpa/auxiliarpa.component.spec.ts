import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuxiliarpaComponent } from './auxiliarpa.component';

describe('AuxiliarpaComponent', () => {
  let component: AuxiliarpaComponent;
  let fixture: ComponentFixture<AuxiliarpaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuxiliarpaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuxiliarpaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
